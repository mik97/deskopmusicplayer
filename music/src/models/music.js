'use strict';

class Music {
    constructor(name, url) {
        Object.defineProperty(this, 'name', {
            enumerable: false,
            configurable: false,
            writable: true,
            value: name || null
        });
        Object.defineProperty(this, 'url', {
            enumerable: false,
            configurable: false,
            writable: true,
            value: url || null
        });
    }
}

module.exports = {Music};